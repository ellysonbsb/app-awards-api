# app-awards-api

Trata-se de uma aplicação que possibilitar a leitura da lista de indicados e vencedores
da categoria Pior Filme do Golden Raspberry Awards.

## Pré Requisito

Para que seja possivel rodar essa aplicação é necessário atender alguns requisitos:
* Java 1.8
* Maven 3.3+

## Tecnologia Utilizada

 * Spring boot
 * Spring Data
 * Swagger
 * Guavas cache
 * lombok
 * H2

## Compilando e Inicializando

Assim como todo projeto Maven, é necessário primeiramente realizarmos a geração de fontes. Conforme o exemplo abaixo:

```bash
mvn clean install
```

Certifique-se de o repositório do Maven está corretamente configurado. Após os fontes terem sido gerados, basta executar o comando abaixo para inicializar a aplicação:

```bash
mvn spring-boot:run
```

## Endpoints

Assim que o projeto estiver sendo executado, é possível verificar as APIs expostas acessando a URL:

```bash
http://localhost:8080/swagger-ui.html#/ 
```