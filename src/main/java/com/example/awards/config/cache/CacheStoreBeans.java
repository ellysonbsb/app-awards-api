package com.example.awards.config.cache;

import com.example.awards.domain.dto.ProducerMinMaxPrizesDTO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class CacheStoreBeans {
    @Bean
    public CacheStore<ProducerMinMaxPrizesDTO> intervalPrizesCache() {
        return new CacheStore<ProducerMinMaxPrizesDTO>(1, TimeUnit.HOURS);
    }
}
