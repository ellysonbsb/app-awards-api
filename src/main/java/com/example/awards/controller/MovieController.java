package com.example.awards.controller;

import com.example.awards.config.cache.CacheStore;
import com.example.awards.controller.mapper.MovieMapper;
import com.example.awards.controller.request.MovieRequest;
import com.example.awards.controller.response.MovieResponse;
import com.example.awards.domain.dto.ProducerMinMaxPrizesDTO;
import com.example.awards.domain.dto.YearWinnerDTO;
import com.example.awards.domain.entity.MovieEntity;
import com.example.awards.domain.service.MovieService;
import com.example.awards.domain.service.ProducerService;
import com.example.awards.domain.service.StudioService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
@CrossOrigin(origins = "*")
@RestController
@RequestMapping(path = "api/v1/movie",
		produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value="API REST Filmes")
@Slf4j
public class MovieController {

	@Autowired
	private MovieService movieService;

	@Autowired
	private ProducerService producerService;

	@Autowired
	private StudioService studioService;

	@Autowired
	private CacheStore<ProducerMinMaxPrizesDTO> intervalPrizesCache;

	private static final String KEY_CACHE_INTERVAL_PRIZES = "intervalPrizes";

	@PostMapping
	public ResponseEntity<MovieResponse> saveMovies(@RequestBody @Valid MovieRequest movieRequest){
		MovieEntity movie = movieService.save(MovieMapper.toMovieEntity(movieRequest));
		producerService.saveProducers(movie, movieRequest.getProducers());
		studioService.saveStudios(movie, movieRequest.getStudios());
		MovieResponse movieResponse = MovieMapper.toResponse(movie, movieRequest.getProducers(), movieRequest.getStudios());
		intervalPrizesCache.removeAll();

		return new ResponseEntity<>(movieResponse, HttpStatus.CREATED) ;
	}

	@GetMapping("/{year}")
    public ResponseEntity<List<MovieResponse>> getMovies(@PathVariable(name="year") Integer year) {
		List<MovieEntity> movies = movieService.getMoviesFromAYear(year);
		
		HttpStatus status = HttpStatus.OK;
		if ( movies.isEmpty() ) {
			status = HttpStatus.NO_CONTENT;
		}
		
        return new ResponseEntity<>(MovieMapper.toListResponse(movies), status ) ;
    }
		@GetMapping("/years")
	public ResponseEntity<YearWinnerDTO> getYearsWithMoreThanOneWinners() {
		YearWinnerDTO dto = movieService.getYearsWithMoreThanOneWinners();

		HttpStatus status = HttpStatus.OK;
		if ( dto.getYears().isEmpty() ) {
			status = HttpStatus.NO_CONTENT;
		}
		
		return new ResponseEntity<YearWinnerDTO>( dto, status ) ;
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> removeMovie(@PathVariable(name="id") Long id) {
		movieService.remove(id);
		intervalPrizesCache.removeAll();
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@GetMapping("/interval-prizes")
	public ResponseEntity<ProducerMinMaxPrizesDTO> getMaxAndMinPrizes() {

		ProducerMinMaxPrizesDTO cache = intervalPrizesCache.get(KEY_CACHE_INTERVAL_PRIZES);
		if(cache != null){
			return new ResponseEntity<>(cache, HttpStatus.OK);
		}

		ProducerMinMaxPrizesDTO dto = producerService.getMaxAndMinPrizes();

		HttpStatus status = HttpStatus.OK;
		if ( dto.getMax().isEmpty() && dto.getMin().isEmpty() ) {
			status = HttpStatus.NO_CONTENT;
		}
		intervalPrizesCache.add(KEY_CACHE_INTERVAL_PRIZES, dto);

		return new ResponseEntity<>(dto, status);
	}

}
