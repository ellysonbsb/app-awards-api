package com.example.awards.controller.mapper;

import com.example.awards.controller.request.MovieRequest;
import com.example.awards.controller.response.MovieResponse;
import com.example.awards.domain.entity.MovieEntity;

import java.util.ArrayList;
import java.util.List;

public class MovieMapper {

    public static List<MovieResponse> toListResponse(List<MovieEntity> entityList){
        List<MovieResponse> list = new ArrayList<>();
        entityList.forEach(d -> list.add(toResponse(d)));
        return list;
    }

    public static MovieResponse toResponse(MovieEntity movie) {
        List<String> studios = new ArrayList<>();
        List<String> producers = new ArrayList<>();
        movie.getProducers().forEach(d -> producers.add(d.getProducer().getName()));
        movie.getStudios().forEach(d -> studios.add(d.getStudio().getName()));
        return MovieResponse.builder()
                .id(movie.getId())
                .winner(movie.getWinner())
                .title(movie.getTitle())
                .year(movie.getYear())
                .producers(producers)
                .studios(studios)
                .build();
    }

    public static MovieEntity toMovieEntity(MovieRequest movie) {
        return MovieEntity.builder()
                .title(movie.getTitle())
                .year(movie.getYear())
                .winner(movie.getWinner()!=null && movie.getWinner())
                .build();

    }

    public static MovieResponse toResponse(MovieEntity movie, List<String> producers, List<String> studios) {
        return MovieResponse.builder()
                .id(movie.getId())
                .winner(movie.getWinner())
                .title(movie.getTitle())
                .year(movie.getYear())
                .producers(producers)
                .studios(studios)
                .build();
    }
}
