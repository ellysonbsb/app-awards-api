package com.example.awards.controller.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MovieRequest implements Serializable {

    @ApiModelProperty(example = "2011", required = true)
    @NotNull(message = "Ano é obrigatório!")
    @Min(value = 999, message = "Ano precisa de 4 digitos!")
    @Max(value = 9999, message = "Ano precisa de 4 digitos!!")
    private Integer year;

    @ApiModelProperty(example = "titulo", required = true)
    @NotBlank(message = "Titulo é obrigatório!")
    @Size(max = 100, message ="Titulo muito longo!")
    private String title;

    @ApiModelProperty(required = true)
    @NotEmpty(message = "Estudio é obrigatório!")
    private List<@NotBlank(message = "Estudio é obrigatório!") String> studios = new ArrayList<>();

    @ApiModelProperty(required = true)
    @NotEmpty(message = "Produtores é obrigatório!")
    private List<@NotBlank(message = "Produtores é obrigatório!")  String> producers = new ArrayList<>();

    private Boolean winner = false;

}
