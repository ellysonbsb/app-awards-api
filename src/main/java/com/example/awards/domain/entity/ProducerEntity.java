package com.example.awards.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="PRODUCER")
public class ProducerEntity {
	
	@Id
	@Column(name="ID_PRODUCER")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="NAME", length=50, nullable=false)
	private String name;
	
	@OneToMany(mappedBy="producer", cascade=CascadeType.ALL, orphanRemoval = true)
	private List<MovieProducerEntity> movies = new ArrayList<>();

}
