package com.example.awards.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="MOVIE_PRODUCER")
public class MovieProducerEntity {
	
	@EmbeddedId
	private MovieProducerKey id;
	
	@ManyToOne
	@MapsId("idMovie")
	private MovieEntity movie;
	
	@ManyToOne
	@MapsId("idProducer")
	private ProducerEntity producer;

	public MovieProducerEntity(MovieEntity movie, ProducerEntity producer) {
		this.movie = movie;
		this.producer = producer;
		this.id = new MovieProducerKey(movie.getId(), producer.getId());
	}

}
