package com.example.awards.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity(name = "Movie")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="MOVIE")
public class MovieEntity implements Serializable {
	
	@Id
	@Column(name="ID_MOVIE")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="YEAR_MOVIE", nullable=false)
	private Integer year;
	
	@Column(name="TITLE", nullable=false)
	private String title;
	
	@Column(name="IS_WINNER", nullable=false)
	private Boolean winner;

	@OneToMany(mappedBy="movie", cascade=CascadeType.ALL, orphanRemoval = true, fetch=FetchType.EAGER)
	private Set<MovieStudioEntity> studios = new HashSet<>();
	
	@OneToMany(mappedBy="movie", cascade=CascadeType.ALL, orphanRemoval = true, fetch=FetchType.EAGER)
	private Set<MovieProducerEntity> producers = new HashSet<>();
	
	public MovieEntity(Integer year, String title, String winner) {
		this.year = year;
		this.title = title;
		this.winner = "yes".equalsIgnoreCase(winner);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, year);
	}
}
