package com.example.awards.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="MOVIE_STUDIO")
public class MovieStudioEntity {

	@EmbeddedId
	private MovieStudioKey id;
	
	@ManyToOne
	@MapsId("idMovie")
	private MovieEntity movie;
	
	@ManyToOne
	@MapsId("idStudio")
	private StudioEntity studio;

	public MovieStudioEntity(MovieEntity movie, StudioEntity studio) {
		this.movie = movie;
		this.studio = studio;
		this.id = new MovieStudioKey(movie.getId(), studio.getId());
	}
}
