package com.example.awards.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class MovieProducerKey implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long idMovie;
	
	private Long idProducer;
}
