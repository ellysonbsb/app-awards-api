package com.example.awards.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name="STUDIO")
public class StudioEntity {
	
	@Id
	@Column(name="ID_STUDIO")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="NAME", length=50, unique=true)
	private String name;
	
	@OneToMany(mappedBy="studio", cascade=CascadeType.ALL, orphanRemoval = true)
	private List<MovieStudioEntity> movies = new ArrayList<>();
}
