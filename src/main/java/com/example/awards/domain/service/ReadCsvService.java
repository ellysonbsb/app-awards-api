package com.example.awards.domain.service;

import com.example.awards.domain.entity.MovieEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

@Service
@Slf4j
public class ReadCsvService {

	@Autowired
	private MovieService movieService;

	@Autowired
	private StudioService studioService;

	@Autowired
	private ProducerService producerService;


	public void processFile() throws IOException {

		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		InputStream in = cl.getResourceAsStream("movielist.csv");
		if(in != null){
			Reader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
			Iterable<CSVRecord> records = CSVFormat.RFC4180
					.builder()
						.setDelimiter(';')
						.setHeader("year","title","studios","producers","winner")
					.build()
					.parse(reader);

			for (CSVRecord record : records) {
				if (record.getRecordNumber() == 1) {
					continue;
				}

				MovieEntity movieNew = new MovieEntity(Integer.valueOf(record.get("year")), record.get("title"), record.get("winner"));
				movieNew = movieService.save(movieNew);

				String studios = record.get("studios");
				studioService.saveStudios(movieNew, studios);

				String producers = record.get("producers");
				producerService.saveProducers(movieNew, producers);
			}
		}
	}

}
