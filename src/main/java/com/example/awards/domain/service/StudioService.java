package com.example.awards.domain.service;

import com.example.awards.domain.entity.MovieEntity;
import com.example.awards.domain.entity.MovieStudioEntity;
import com.example.awards.domain.entity.StudioEntity;
import com.example.awards.domain.repository.MovieStudioRepository;
import com.example.awards.domain.repository.StudioRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Log
public class StudioService {

	@Autowired
	private StudioRepository studioRepository;
	
	@Autowired
	private MovieStudioRepository movieStudioRepository;
	
	public void saveStudios(MovieEntity movie, String studios) {
		for (String strStudio : studios.split(",|\\ and ")) {
			StudioEntity studio = StudioEntity.builder().name(strStudio.trim()).build();
			save(studio, movie);
		}
	}
	public void saveStudios(MovieEntity movie, List<String> studios) {
		for(String strStudio : studios){
			StudioEntity studio = StudioEntity.builder().name(strStudio.trim()).build();
			save(studio, movie);
		}
	}

	private void save(StudioEntity studio, MovieEntity movie) {
		Example<StudioEntity> example = Example.of(studio);

		if (studioRepository.exists(example)) {
			studio = studioRepository.findByName(studio.getName());
		} else {
			studio = studioRepository.save(studio);
		}

		movieStudioRepository.save(new MovieStudioEntity(movie, studio));
	}

}
