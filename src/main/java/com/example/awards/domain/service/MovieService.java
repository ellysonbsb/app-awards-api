package com.example.awards.domain.service;

import com.example.awards.domain.dto.YearWinnerDTO;
import com.example.awards.domain.dto.YearWinnerMovieDTO;
import com.example.awards.domain.entity.MovieEntity;
import com.example.awards.domain.exception.EntidadeNaoEncontradaException;
import com.example.awards.domain.repository.MovieRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Log
public class MovieService {

	@Autowired
	private MovieRepository movieRepository;

	public MovieEntity save(MovieEntity movie) {
		return movieRepository.save(movie);
	}
	
	public List<MovieEntity> getMoviesFromAYear(Integer year) {
		return movieRepository.findByYear(year);
	}
	
	
	public YearWinnerDTO getYearsWithMoreThanOneWinners() {
		List<YearWinnerMovieDTO> years = movieRepository.findYearsWithModeThanOneWinner();
		if (years == null || years.isEmpty()) {
			return new YearWinnerDTO();
		}
		return new YearWinnerDTO(years);
	}

	public void remove(Long id) {
		Optional<MovieEntity> optional = movieRepository.findById(id);
		
		if ( !optional.isPresent() ) {
			throw new EntidadeNaoEncontradaException("Não foi encontrado o filme.");
		}
		
		MovieEntity movie = optional.get();
		
		movieRepository.delete(movie);
	}
	
}
