package com.example.awards.domain.service;

import com.example.awards.domain.dto.ProducerMinMaxPrizesDTO;
import com.example.awards.domain.dto.ProducerPrizesDTO;
import com.example.awards.domain.entity.MovieEntity;
import com.example.awards.domain.entity.MovieProducerEntity;
import com.example.awards.domain.entity.ProducerEntity;
import com.example.awards.domain.repository.MovieProducerRepository;
import com.example.awards.domain.repository.ProducerRepository;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import springfox.documentation.annotations.Cacheable;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ProducerService {
	
	Logger logger = LoggerFactory.getLogger(ProducerService.class);
	
	@Autowired
	private ProducerRepository producerRepository;
	
	@Autowired
	private MovieProducerRepository movieProducerRepository;
	
	public void saveProducers(MovieEntity movie, String producers) {
		for (String strProducer : producers.split(",|\\ and ")) {
			ProducerEntity producer = ProducerEntity.builder().name(strProducer.trim()).build();
			save(producer, movie);
		}
	}

	public void saveProducers(MovieEntity movie, List<String> producers) {
		for(String strProducer : producers){
			ProducerEntity producer = ProducerEntity.builder().name(strProducer.trim()).build();
			save(producer, movie);
		}
	}

	private void save(ProducerEntity producer, MovieEntity movie) {
		Example<ProducerEntity> example = Example.of(producer);

		if (producerRepository.exists(example)) {
			producer = producerRepository.findByName(producer.getName());
		} else {
			producer = producerRepository.save(producer);
		}

		movieProducerRepository.save(new MovieProducerEntity(movie, producer));
	}


	@Cacheable("intervalPrizes")
	public ProducerMinMaxPrizesDTO getMaxAndMinPrizes() {
		log.info("iniciando a busca de intervalo");
		List<MovieProducerEntity> mpList = movieProducerRepository.findByMovieWinnerOrderByProducerId(true);

		List<ProducerPrizesDTO> min = findMinInterval(mpList);
		List<ProducerPrizesDTO> max = findMaxInterval(mpList);
		
		return ProducerMinMaxPrizesDTO.builder().min(min).max(max).build();
	}

	private List<ProducerPrizesDTO> findMinInterval(List<MovieProducerEntity> mpList) {
		List<ProducerPrizesDTO> min = new ArrayList<>();
		Integer maxValue = Integer.MAX_VALUE;
		
		for ( int i = 0; i < mpList.size() - 1; i++ ) {
			
			for (int j = i + 1; j < mpList.size(); j++) {

				MovieProducerEntity mpi = mpList.get(i);
				MovieProducerEntity mpj = mpList.get(j);
				
				if (mpi.getProducer().equals(mpj.getProducer())) {
					Integer interval = Math.abs(mpi.getMovie().getYear()-mpj.getMovie().getYear());
					
					if (interval <= maxValue) {
						min = interval.equals(maxValue) ? min : new ArrayList<>();
						maxValue = interval;
						min.add(ProducerPrizesDTO.builder()
										.interval(interval)
										.producer(mpi.getProducer().getName())
										.previousWin(mpi.getMovie().getYear())
										.followingWin(mpj.getMovie().getYear())
								.build());
					}
				}
			}
		}
		
		return min;
	}
	
	private List<ProducerPrizesDTO> findMaxInterval(List<MovieProducerEntity> mpList) {
		List<ProducerPrizesDTO> max = new ArrayList<>();
		Integer minValue = Integer.MIN_VALUE;
		
		for ( int i = 0; i < mpList.size() - 1; i++ ) {
			
			for (int j = i + 1; j < mpList.size(); j++) {

				MovieProducerEntity mpi = mpList.get(i);
				MovieProducerEntity mpj = mpList.get(j);
				
				if (mpi.getProducer().equals(mpj.getProducer())) {
					Integer interval = Math.abs(mpi.getMovie().getYear()-mpj.getMovie().getYear());
					
					if (interval >= minValue) {
						max = interval.equals(minValue) ? max : new ArrayList<>();
						minValue = interval;
						max.add(ProducerPrizesDTO.builder()
								.interval(interval)
								.producer(mpi.getProducer().getName())
								.previousWin(mpi.getMovie().getYear())
								.followingWin(mpj.getMovie().getYear())
						.build());
						break;
					}
				}
			}
		}
		
		return max;
	}

}
