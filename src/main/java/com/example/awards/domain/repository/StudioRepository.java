package com.example.awards.domain.repository;

import com.example.awards.domain.dto.StudioWinDTO;
import com.example.awards.domain.entity.StudioEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudioRepository extends JpaRepository<StudioEntity, Long>{
	
	StudioEntity findByName(String name);
	
	@Query(value="select new com.example.awards.domain.dto.StudioWinDTO(studio.name, count(movie.winner)) "
			+ "from MovieStudioEntity as ms join ms.movie as movie join ms.studio as studio "
			+ "where movie.winner=true group by studio.name order by 2 desc")
	List<StudioWinDTO> findByWinners();

}
