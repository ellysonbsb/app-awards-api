package com.example.awards.domain.repository;

import com.example.awards.domain.entity.MovieStudioEntity;
import com.example.awards.domain.entity.MovieStudioKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieStudioRepository extends JpaRepository<MovieStudioEntity, MovieStudioKey>{

}
