package com.example.awards.domain.repository;

import com.example.awards.domain.entity.ProducerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProducerRepository extends JpaRepository<ProducerEntity, Long> {
	
	ProducerEntity findByName(String name);
	
}
