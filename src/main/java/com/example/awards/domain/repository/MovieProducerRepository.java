package com.example.awards.domain.repository;

import com.example.awards.domain.entity.MovieProducerEntity;
import com.example.awards.domain.entity.MovieProducerKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MovieProducerRepository extends JpaRepository<MovieProducerEntity, MovieProducerKey> {
	
	@Query(value="select mp from MovieProducerEntity as mp join mp.movie as movie join mp.producer as producer "
			+ "where movie.winner = true order by producer.id, movie.year")
	List<MovieProducerEntity> findByMovieWinnerOrderByProducerId(Boolean isWinner);
	
}
