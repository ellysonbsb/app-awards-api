package com.example.awards.domain.repository;

import com.example.awards.domain.dto.YearWinnerMovieDTO;
import com.example.awards.domain.entity.MovieEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MovieRepository extends JpaRepository<MovieEntity, Long> {
	
	List<MovieEntity> findByYear(Integer year);
	
	@Query(value="select new com.example.awards.domain.dto.YearWinnerMovieDTO(movie.year, count(movie.winner)) "
			+ "from Movie as movie where movie.winner=true group by movie.year having count(movie.winner) > 1")
	List<YearWinnerMovieDTO> findYearsWithModeThanOneWinner();
	
}
