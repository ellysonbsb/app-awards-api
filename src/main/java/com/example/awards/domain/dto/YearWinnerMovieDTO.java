package com.example.awards.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class YearWinnerMovieDTO {
	
	private Integer year;
	
	private Long winnerCount;

}
