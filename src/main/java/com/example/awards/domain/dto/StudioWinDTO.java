package com.example.awards.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudioWinDTO {
	
	private String name;
	
	private Long winCount;
}
