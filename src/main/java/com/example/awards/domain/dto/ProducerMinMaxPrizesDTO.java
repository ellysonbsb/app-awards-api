package com.example.awards.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProducerMinMaxPrizesDTO {
	
	private List<ProducerPrizesDTO> min = new ArrayList<>();
	
	private List<ProducerPrizesDTO> max = new ArrayList<>();

}
