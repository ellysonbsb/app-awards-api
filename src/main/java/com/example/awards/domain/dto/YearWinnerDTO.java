package com.example.awards.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class YearWinnerDTO {
	
	private List<YearWinnerMovieDTO> years;

}
