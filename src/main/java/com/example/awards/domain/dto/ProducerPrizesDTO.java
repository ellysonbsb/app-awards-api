package com.example.awards.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProducerPrizesDTO {
	
	private String producer;
	
	private Integer interval;
	
	private Integer previousWin;
	
	private Integer followingWin;
	
}
