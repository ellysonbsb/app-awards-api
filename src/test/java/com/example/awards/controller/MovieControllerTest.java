package com.example.awards.controller;


import com.example.awards.controller.request.MovieRequest;
import com.example.awards.controller.response.MovieResponse;
import com.example.awards.domain.dto.ProducerMinMaxPrizesDTO;
import com.example.awards.domain.dto.YearWinnerDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MovieControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @BeforeEach
    void setup() throws IOException {
    }

    @Test
    void getMoviesTest() {
        ResponseEntity<List<MovieResponse>> response = this.testRestTemplate.exchange("/api/v1/movie/1980", HttpMethod.GET, null, new ParameterizedTypeReference<List<MovieResponse>>() {
        });
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertEquals(response.getBody().size(), 10);
    }

    @Test
    void getMoviesEmptyTest() {
        ResponseEntity<List<MovieResponse>> response = this.testRestTemplate.exchange("/api/v1/movie/2024", HttpMethod.GET, null, new ParameterizedTypeReference<List<MovieResponse>>() {
        });
        assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);
    }

    @Test
    void getYearsWithMoreThanOneWinnersTest() {
        ResponseEntity<YearWinnerDTO> response = this.testRestTemplate.exchange("/api/v1/movie/years", HttpMethod.GET, null, YearWinnerDTO.class);

        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertEquals(response.getBody().getYears().size(), 3);
    }

    @Test
    void removeMovieTest() {
        ResponseEntity<Void> response = this.testRestTemplate.exchange("/api/v1/movie/3", HttpMethod.DELETE, null, Void.class);

        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    void removeMovieErrorTest() {
        ResponseEntity<Void> response = this.testRestTemplate.exchange("/api/v1/movie/350697", HttpMethod.DELETE, null, Void.class);

        assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    void intervalPrizesTest() {
        ResponseEntity<ProducerMinMaxPrizesDTO> response = this.testRestTemplate.exchange("/api/v1/movie/interval-prizes", HttpMethod.GET, null, ProducerMinMaxPrizesDTO.class);

        ProducerMinMaxPrizesDTO producer = response.getBody();
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertEquals(producer.getMax().get(0).getInterval(), 13);
        assertEquals(producer.getMin().get(0).getInterval(), 1);
    }

    @Test
    void saveTest() {
        MovieRequest body = MovieRequest.builder()
                .title("Teste")
                .year(2020)
                .winner(true)
                .producers(Arrays.asList("Teste"))
                .studios(Arrays.asList("Studio"))
                .build();
        HttpEntity<MovieRequest> request = new HttpEntity<>(body);
        ResponseEntity<MovieResponse> response = this.testRestTemplate.exchange("/api/v1/movie", HttpMethod.POST, request, MovieResponse.class);

        assertEquals(response.getStatusCode(), HttpStatus.CREATED);
        assertNotNull(response.getBody().getId());
    }

    @Test
    void saveWinnerDefaultTest() {
        MovieRequest body = MovieRequest.builder()
                .title("Teste")
                .year(2020)
                .producers(Arrays.asList("Teste"))
                .studios(Arrays.asList("Studio"))
                .build();
        HttpEntity<MovieRequest> request = new HttpEntity<>(body);
        ResponseEntity<MovieResponse> response = this.testRestTemplate.exchange("/api/v1/movie", HttpMethod.POST, request, MovieResponse.class);

        assertEquals(response.getStatusCode(), HttpStatus.CREATED);
        assertFalse(response.getBody().getWinner());
    }

    @Test
    void saveTitleEmptyTest() {
        MovieRequest body = MovieRequest.builder()
                .title("")
                .year(2020)
                .producers(Arrays.asList("Teste"))
                .studios(Arrays.asList("Studio"))
                .build();
        HttpEntity<MovieRequest> request = new HttpEntity<>(body);
        ResponseEntity<MovieResponse> response = this.testRestTemplate.exchange("/api/v1/movie", HttpMethod.POST, request, MovieResponse.class);

        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test
    void saveProduceEmptyTest() {
        MovieRequest body = MovieRequest.builder()
                .title("Test")
                .year(2020)
                .producers(Arrays.asList(""))
                .studios(Arrays.asList("Studio"))
                .build();
        HttpEntity<MovieRequest> request = new HttpEntity<>(body);
        ResponseEntity<MovieResponse> response = this.testRestTemplate.exchange("/api/v1/movie", HttpMethod.POST, request, MovieResponse.class);

        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test
    void saveStudioEmptyTest() {
        MovieRequest body = MovieRequest.builder()
                .title("Test")
                .year(2020)
                .producers(Arrays.asList("producers"))
                .studios(Arrays.asList(""))
                .build();
        HttpEntity<MovieRequest> request = new HttpEntity<>(body);
        ResponseEntity<MovieResponse> response = this.testRestTemplate.exchange("/api/v1/movie", HttpMethod.POST, request, MovieResponse.class);

        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }


    @Test
    void saveYearEmptyTest() {
        MovieRequest body = MovieRequest.builder()
                .title("Test")
                .producers(Arrays.asList("producers"))
                .studios(Arrays.asList("studios"))
                .build();
        HttpEntity<MovieRequest> request = new HttpEntity<>(body);
        ResponseEntity<MovieResponse> response = this.testRestTemplate.exchange("/api/v1/movie", HttpMethod.POST, request, MovieResponse.class);

        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

}
